console.log('Hello World');

// Functions

// [SECTION] Parameters and Arguments

    // Functions in JS are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.

    // (name) this is parameter
    function printName(name) {
        console.log('My name is ' + name);
        console.log('Hello, ' + name);
    };

    // 'Juana' this is argument
    printName('Juana');

    // DRY Principle - Dont Repeat Yourself

    let sampleVariable = 'Yui';
    printName(sampleVariable);

        // Let's try with our previous activity!

        function checkDivisibilityBy8(num) {
            let remainder = num % 8;
            console.log('The remainder of ' + num + ' divided by 8 is: ' + remainder);
            let isDivisibilityBy8 = remainder === 0;
            console.log('Is ' + num + ' divisible by 8?');
            console.log(isDivisibilityBy8);
        };

        checkDivisibilityBy8(28);
        checkDivisibilityBy8(64);

// [SECTION] Functions as Arguments

    function argumentFunction() {
        console.log('This function was passed as an argument before the message was printed.');

    };

    // parameter always acts like a variable
    // parameter is like a catcher
    // this is a placeholder/storage
    function invokeFunction(iAmNotRelated) {
        iAmNotRelated();
    };

    invokeFunction(argumentFunction);

    // [SUB-SECTION] Using Multiple Parameters

        // Multiple 'arguments' will correspond to the number of 'parameters' declared in a function in succeeding order.

        function createFullName(firstName, middleName, lastName) {
            console.log(firstName + ' ' + middleName + ' ' + lastName);
        };

        createFullName('Juan', 'Dela', 'Cruz');

        // In JS, providing more/less arguments than the expected parameters will not return an error.

        // If we provide less arguments than expected, this automatically assign an undefined value to the parameter.

        createFullName('Juan', 'Dela');
        createFullName('Juan', 'Dela', 'Cruz', 'Jr.');

    // [SUB-SECTION] Using Variables as Arguments

        let firstName = 'John';
        let middleName = 'Doe';
        let lastName = 'Smith';

        createFullName(firstName, middleName, lastName);


    // Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

    function printFullName(middleName, firstName, lastName) {
        console.log(firstName + ' ' + middleName + ' ' + lastName);
    };

    printFullName('Juan', 'Crisostomo', 'Ibarra');

    // Results to 'Crisostomo Juan Ibarra' because 'Juan' was received as middleName, 'Crisostomo' was received as firstName.

// [SECTION] Return Statement

    // The 'return' statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.
    
    function returnFullName(firstName, middleName, lastName) {
        console.log('This message will be printed');
        return firstName + ' ' + middleName + ' ' + lastName;
        console.log('Try this one out!');
    };

    let completeName = returnFullName('F', 'P', 'J');
    console.log(completeName);

    let anotherName = returnFullName('Jeffrey', 'Smith', 'Bezos');
    console.log(anotherName);

    // This way, a function is able to return a value we can further use/manipulate in our program instead of only printing/displaying it in the console.

    // You can also create a variable inside the function to contain the result and return that variable instead.

    function returnAddress(city, country) {
        let fullAddress = city + ', ' + country + ' !';
        console.log('Welcome to Cebu!')
        return fullAddress; 
    };

    let myAddress = returnAddress('Cebu City','Cebu');
    console.log(myAddress);

    // On the other hand, when a function has console.log() to display.

    // [SUB-SECTION] console.log vs return statement
    function printPlayerInfo(username, level, job) {
        console.log('Username: ' + username);
        console.log('Level: ' + level);
        console.log('Job: ' + job);

        // let playerName = 'Username: ' + username;
        // return playerName;
    };

    let user1 = printPlayerInfo('knight_white', 95, 'Paladin');
    console.log(user1);

    // returns undefined because printPlayerInfo returns nothing. It only console.logs the detals.

    // You cant save any value from printPlayerInfo() because it doesnt return anything.